export const up = (knex, Promise) =>
  knex.schema
    .createTable('types', (table) => {
      table.integer('id').unique().notNullable()
      table.string('type').notNullable()
    })
    .createTable('products', (table) => {
      table.string('id').unique().notNullable()
      table.string('name').notNullable()
      table.text('description')
      table.float('price').notNullable()
      table.integer('amount').notNullable()
      table.integer('min_amount')
      table.integer('type_id').notNullable()
      table.foreign('type_id').references('id').inTable('types')
      table.timestamps()
    })
    .createTable('users', (table) => {
      table.uuid('id').unique().notNullable()
      table.string('email').unique().notNullable()
      table.string('name')
      table.string('username').unique().notNullable()
      table.string('password').notNullable()
      table.timestamps()
    })

export const down = (knex, Promise) =>
  knex.schema
    .dropTableIfExists('products')
    .dropTableIfExists('types')
    .dropTableIfExists('users')
