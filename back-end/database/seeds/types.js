export const seed = async (knex, Promise) => {
  await knex('types').del()
  await knex('types').insert([
    { id: 1, type: 'Perecivel' },
    { id: 2, type: 'Limpeza' },
    { id: 3, type: 'Cama, roupa e banho' },
    { id: 4, type: 'Padaria' },
    { id: 5, type: 'Carnes' }
  ])
}
