import bookshelf from '../bookshelf'
import Type from './type'

export default bookshelf.Model.extend({
  tableName: 'products',
  hasTimestamps: true,
  type_id: function () {
    this.belongsTo(Type)
  }
})
