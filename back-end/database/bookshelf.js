import { development } from './knexfile'
import Knex from 'knex'
import Bookshelf from 'bookshelf'

const knex = Knex(development)
const bookshelf = Bookshelf(knex)

bookshelf.plugin(require('bookshelf-uuid'))

export default bookshelf
