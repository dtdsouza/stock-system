import Koa from 'koa'
import koaBody from 'koa-body'
import koaLogger from 'koa-logger'
import router from './routes/index'
import jwt from 'koa-jwt'
import jwtChecker from './middlewares/jwt.middleware'

require('dotenv').config()

const app = new Koa()

app.use(async (ctx, next) => {
  try {
    await next()
  } catch (error) {
    ctx.status = error.statusCode || 500
    ctx.body = { error: error.message || error.toString() }
  }
})

app.use(jwt({
  secret: process.env.JWT_SECRET,
  jwtChecker
}).unless({
  path: [
    '/public'
  ]
}))

app
  .use(koaLogger())
  .use(koaBody())
  .use(router.routes())
  .use(router.allowedMethods())

export default app
