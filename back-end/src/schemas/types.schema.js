import validate from 'koa-joi-validate'
import Joi from 'joi'

class Validate {
  create () {
    return validate({
      body: {
        id: Joi.number().integer().required(),
        type: Joi.string().required()
      }
    })
  }

  update () {
    return validate({
      body: {
        id: Joi.number().integer().required(),
        type: Joi.string().required()
      }
    })
  }
}

export default Validate
