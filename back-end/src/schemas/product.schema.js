import validate from 'koa-joi-validate'
import Joi from 'joi'

class Validate {
  create () {
    return validate({
      body: {
        id: Joi.string().required(),
        name: Joi.string().required(),
        description: Joi.string(),
        price: Joi.number().required(),
        amount: Joi.number().integer().required(),
        min_amount: Joi.number().integer(),
        type_id: Joi.number().integer().required()
      }
    })
  }

  update () {
    return validate({
      body: {
        name: Joi.string().required(),
        description: Joi.string(),
        price: Joi.number().required(),
        amount: Joi.number().integer().required(),
        min_amount: Joi.number().integer(),
        type_id: Joi.number().integer().required()
      }
    })
  }
}

export default Validate
