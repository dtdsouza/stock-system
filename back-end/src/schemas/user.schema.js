import validate from 'koa-joi-validate'
import Joi from 'joi'

class Validate {
  create () {
    return validate({
      body: {
        email: Joi.string().email().required(),
        name: Joi.string(),
        username: Joi.string().required(),
        password: Joi.string().required()
      }
    })
  }

  update () {
    return validate({
      body: {
        email: Joi.string().email().required(),
        name: Joi.string(),
        username: Joi.string().required(),
        password: Joi.string().required()
      }
    })
  }

  login () {
    return validate({
      body: {
        email: Joi.string().email().required(),
        password: Joi.string().required()
      }
    })
  }
}

export default Validate
