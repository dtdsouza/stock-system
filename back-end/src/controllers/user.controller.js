import User from './../../database/models/user'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import {
  NotFound,
  InternalServerError,
  BadRequest,
  deleted,
  Unauthorized
} from './../utils/errorHandler'

require('dotenv').config()

export default class Controller {
  async login (ctx) {
    const user = await new User({ email: ctx.request.body.email })
      .fetch()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!user) { throw new NotFound('Usuario nao encontrado') }

    const valid = await bcrypt.compare(ctx.request.body.password, user.attributes.password)

    if (!valid) { throw new Unauthorized('Wrong Password') }
    const { id, username } = user
    user.attributes.token = jwt.sign({ sub: { id, username } }, process.env.JWT_SECRET)

    ctx.body = user
  }

  async get (ctx) {
    const user = await new User()
      .fetchAll()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    ctx.body = user
  }

  async getById (ctx, next) {
    const user = await new User({ id: ctx.params.id })
      .fetch()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!user) { throw new NotFound('Element not found') } else { ctx.body = user }
  }

  async insert (ctx, next) {
    const hash = await bcrypt.hash(ctx.request.body.password, 10)

    const user = await new User()
      .save({
        email: ctx.request.body.email,
        name: ctx.request.body.name,
        username: ctx.request.body.username,
        password: hash
      })
      .catch((err) => { throw new InternalServerError(err.message) })

    if (!user) { throw new BadRequest() } else { ctx.body = user }
  }

  async update (ctx) {
    const { body } = ctx.request
    console.log('here')
    if (body.password) {
      body.password = await bcrypt.hash(body.password, 10)
    }
    console.log('here')

    const user = await new User({ id: ctx.params.id })
      .save({
        name: body.name,
        email: body.email,
        password: body.password
      }, { method: 'update' })
      .catch(() => { throw new NotFound('User Not Found') })

    if (!user) { throw new NotFound('User Not found') } else { ctx.body = user }
  }

  async delete (ctx, next) {
    const user = await new User()
      .where({ id: ctx.params.id })
      .destroy()
      .catch(() => { throw new NotFound('User Not Found') })

    if (!user) { throw new NotFound('User Not found') } else { deleted(ctx) }
  }
}
