import Product from './../../database/models/product'
import {
  NotFound,
  InternalServerError,
  BadRequest,
  deleted
} from './../utils/errorHandler'

export default class Controller {
  async get (ctx, next) {
    const products = await new Product()
      .fetchAll()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    ctx.body = products
  }

  async getById (ctx, next) {
    const product = await new Product({ id: ctx.params.id })
      .fetch()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!product) { throw new NotFound('element not found') } else { ctx.body = product }
  }

  async insert (ctx, next) {
    const product = await new Product()
      .save({
        id: ctx.request.body.id,
        name: ctx.request.body.name,
        description: ctx.request.body.description,
        price: ctx.request.body.price,
        amount: ctx.request.body.amount,
        min_amount: ctx.request.body.min_amount,
        type_id: ctx.request.body.type_id
      })
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!product) { throw new BadRequest() } else { ctx.body = product }
  }

  async update (ctx, next) {
    const product = await new Product()
      .where({ id: ctx.params.id })
      .save({
        id: ctx.params.id,
        name: ctx.request.body.name,
        description: ctx.request.body.description,
        price: ctx.request.body.price,
        amount: ctx.request.body.amount,
        min_amount: ctx.request.body.min_amount,
        type_id: ctx.request.body.type_id
      }, { method: 'update' })
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!product) { throw new NotFound('element not found') } else { ctx.body = product }
  }

  async delete (ctx, next) {
    const product = await new Product()
      .where({ id: ctx.params.id })
      .destroy()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!product) { throw new NotFound('element not found') } else { deleted(ctx) }
  }
}
