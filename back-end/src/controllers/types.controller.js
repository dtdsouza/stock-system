import Type from './../../database/models/type'
import {
  NotFound,
  InternalServerError,
  deleted,
  BadRequest
} from './../utils/errorHandler'

export default class Controller {
  async get (ctx) {
    const type = await new Type()
      .fetchAll()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    ctx.body = type
  }

  async getById (ctx) {
    const type = await new Type({ id: ctx.params.id })
      .fetch()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!type) { throw new NotFound('Element not found') } else { ctx.body = type }
  }

  async insert (ctx) {
    const type = await new Type()
      .save({
        id: ctx.request.body.id,
        type: ctx.request.body.type
      })
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!type) { throw new BadRequest(ctx) } else { ctx.body = type }
  }

  async update (ctx) {
    const type = await new Type()
      .where({ id: ctx.params.id })
      .save({
        id: ctx.params.id,
        type: ctx.request.body.type
      }, { method: 'update' })
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!type) { throw new NotFound('Element not found') } else { ctx.body = type }
  }

  async delete (ctx) {
    const type = await new Type({ id: ctx.params.id })
      .destroy()
      .catch((err) => { throw new InternalServerError(err.toString()) })

    if (!type) { throw new NotFound('element not Found') } else { deleted(ctx) }
  }
}
