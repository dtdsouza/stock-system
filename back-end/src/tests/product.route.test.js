const request = require('supertest')
const app = require('./../app')

let server
const product = {
  code: '1a',
  name: 'teste',
  description: 'lorem ipsum loren ipsum',
  amount: 15,
  min_amount: 5,
  type_id: 2
}

describe('Test the products path', () => {
  beforeEach(async () => {
    server = app.listen()
  })

  afterEach(async () => {
    server.close()
  })

  test('Simple Get All', async (done) => {
    const response = await request(server).get('/products')
    expect(response.statusCode).toEqual(200)
    expect(response.body.length).toBeGreaterThanOrEqual(0)
    done()
  })

  test('Insert a test Product', async (done) => {
    const response = await request(server)
      .post('/products')
      .send(product)
    expect(response.statusCode).toEqual(200)
    done()
  })

  test('Get by valid Code', async (done) => {
    const response = await request(server).get('/products/1a')
    expect(response.statusCode).toEqual(200)
    done()
  })

  test('Get by not valid Code', async (done) => {
    const response = await request(server).get('/products/12121')
    expect(response.statusCode).toEqual(404)
    done()
  })

  test('Update a test Product', async (done) => {
    product.amount = 20
    const response = await request(server)
      .put('/products/1a')
      .send(product)
    expect(response.statusCode).toEqual(200)
    done()
  })

  test('Get by valid Code', async (done) => {
    const response = await request(server).get('/products/1a')
    product.created_at = null
    product.updated_at = null
    expect(response.statusCode).toEqual(200)
    expect(response.body).toEqual(product)
    done()
  })

  test('Delete test product', async (done) => {
    const response = await request(server).delete('/products/1a')
    expect(response.statusCode).toEqual(200)
    done()
  })
})
