import users from './user.route'
import types from './types.route'
import products from './products.route'
import Router from 'koa-router'

const router = new Router()

router.use(users.routes())
router.use(types.routes())
router.use(products.routes())

export default router
