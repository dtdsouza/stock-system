import Router from 'koa-router'
import Controller from './../controllers/types.controller'
import Validator from './../schemas/types.schema'

const typeValidator = new Validator()
const ctrlTypes = new Controller()
const router = new Router()

router
  .get('/types', ctrlTypes.get)
  .get('/types/:id', ctrlTypes.getById)
  .post('/types', typeValidator.create(), ctrlTypes.insert)
  .put('/types/:id', typeValidator.update(), ctrlTypes.update)
  .delete('/types/:id', ctrlTypes.delete)

export default router
