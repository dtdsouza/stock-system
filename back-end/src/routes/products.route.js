import Router from 'koa-router'
import Controller from '../controllers/products.controller'
import ProductValidator from './../schemas/product.schema'

const validate = new ProductValidator()
const router = new Router()
const ctrlProducts = new Controller()

router
  .get('/products', ctrlProducts.get)
  .get('/products/:id', ctrlProducts.getById)
  .post('/products', validate.create(), ctrlProducts.insert)
  .put('/products/:id', validate.update(), ctrlProducts.update)
  .delete('/products/:id', ctrlProducts.delete)

export default router
