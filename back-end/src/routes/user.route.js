import Router from 'koa-router'
import Controller from '../controllers/user.controller'
import Validator from './../schemas/user.schema'

const router = new Router()
const ctrlUsers = new Controller()
const userValidate = new Validator()

router
  .get('/users', ctrlUsers.get)
  .get('/users/:id', ctrlUsers.getById)
  .post('/users', userValidate.create(), ctrlUsers.insert)
  .post('/users/login', userValidate.login(), ctrlUsers.login)
  .put('/users/:id', userValidate.update(), ctrlUsers.update)
  .delete('/users/:id', ctrlUsers.delete)

export default router
