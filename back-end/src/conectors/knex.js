import { development } from './../../database/knexfile'
import { notFound } from './../utils/errorHandler'

import Knex from 'knex'

export const knex = Knex(development)

export const db = {
  simpleGetAll: async (table) => {
    return knex(table).select()
  },
  getBySpecificId: async (table, column, value) => {
    const response = await knex(table).select().where(column, value)
    if (response.length <= 0) { throw notFound('Element Not Found') } else return response
  },
  insert: async (table, data) => {
    return knex(table).insert(data)
  },
  update: async (table, data, column, value) => {
    const response = await knex(table).update(data).where(column, value)
    if (response === 0) { throw notFound('Element does not exists') } else return response
  },
  delete: async (table, column, value) => {
    const response = await knex(table).delete().where(column, value)
    if (response === 0) { throw notFound('Element does not exists') } else return response
  }
}
