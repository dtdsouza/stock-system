export const NotFound = (message) => {
  const error = new Error()
  error.message = message || 'Not Found'
  error.statusCode = 404

  return error
}

export const BadRequest = (message) => {
  const error = new Error()
  error.message = message || 'Bad Request'
  error.statusCode = 400

  return error
}

export const InternalServerError = (message) => {
  const error = new Error()
  error.message = message || 'Internal Server Error'
  error.statusCode = 500

  return error
}

export const Unauthorized = (message) => {
  const error = new Error()
  error.message = message || 'Unauthorized'
  error.statusCode = 401

  return error
}

export const deleted = (ctx, message) => {
  ctx.body = { error: message || 'Successfully deleted' }
  ctx.status = 200
}
